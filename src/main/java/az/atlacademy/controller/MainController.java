package az.atlacademy.controller;

import az.atlacademy.model.Trainer;
import az.atlacademy.repository.TrainerRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;


@Controller
public class MainController {
    private TrainerRepository trainerRepository;

    public MainController(TrainerRepository trainerRepository) {
        this.trainerRepository = trainerRepository;
    }

    private String name = "ATL Academy";

    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public String about() {
        return "about";
    }

    @GetMapping(value = "/")
    public ModelAndView home() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("home");
        modelAndView.addObject("name", name);
        modelAndView.addObject("now", LocalDateTime.now());
        return modelAndView;
    }

    @RequestMapping(value = "/member/{id}", method = RequestMethod.GET)
    public ModelAndView member(@PathVariable Long id) {
        Trainer trainer = trainerRepository.findById(id).get();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("member");
        modelAndView.addObject("trainer",trainer );
        return modelAndView;
    }

    @PostMapping("/member")
    public String createTrainer(@ModelAttribute Trainer trainer) {
        System.out.println(trainer);
        System.out.println(trainerRepository.save(trainer));
        return "redirect:/";
    }

    @GetMapping("/member/form")
    public ModelAndView formTrainer() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("form_trainer");
        modelAndView.addObject("trainer", new Trainer());
        return modelAndView;
    }
}
