package az.atlacademy.controller;

import az.atlacademy.model.Trainer;
import az.atlacademy.repository.TrainerRepository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.DriverManager;
import java.sql.SQLException;

@RestController
@RequestMapping("/api/v1")
public class RestApiController {
    private TrainerRepository trainerRepository;

    public RestApiController(TrainerRepository trainerRepository) {
        this.trainerRepository = trainerRepository;
    }

    @RequestMapping(value = "/member/{id}", method = RequestMethod.GET)
    public Trainer member(@PathVariable Long id) {
        Trainer trainer = trainerRepository.findById(id).get();

            return trainer;
    }
}
